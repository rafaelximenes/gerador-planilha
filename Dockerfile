FROM openjdk:8-jdk-alpine
ENV HTTP_PROXY="http://10.1.101.101:8080"
ENV PIP_OPTIONS="--proxy $HTTP_PROXY"
ENV HTTPS_PROXY http://10.1.101.101:8080/
ENV FTP_PROXY http://10.1.101.101:8080
ENV NO_PROXY localhost,127.0.0.0/8,::1,10.0.0.0/8,*backend*,*.presidencia.gov.br,*.planalto.gov.br
VOLUME /tmp
ARG JAR_FILE
COPY /target/colegiados-0.0.1-SNAPSHOT.jar app.jar
#RUN apk add tzdata
#RUN cp /usr/share/zoneinfo/Brazil/East /etc/localtime
#RUN apk del tzdata
ENTRYPOINT ["java","-jar","/app.jar"]
