package planilha.colegiados;

import javax.mail.internet.MimeMessage;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

@Service
public interface EmailService {
	
	public void enviarEmail(String to, String from, String subject, String msg);
	
	
	public void enviarEmailToSystemAdministrator(String subject, String msg);
	
	public void sendMail(SimpleMailMessage smm);
	
	void sendHtmlEmail(MimeMessage msg);

	
	

}
