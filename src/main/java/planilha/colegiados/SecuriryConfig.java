package planilha.colegiados;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;


@EnableWebSecurity
@Configuration
public class SecuriryConfig extends WebSecurityConfigurerAdapter {
	
	private static final String[] PUBLIC_MATCHERS = {
			"/planilha/**",
			"/h2-console/**"
	};
	
	@Autowired
    private Environment env;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	
    	if(Arrays.asList(env.getActiveProfiles()).contains("test")) {
			http.headers().frameOptions().disable();
		}
    	
    	http.cors().and().csrf().disable();
    	http.authorizeRequests()
		.antMatchers(HttpMethod.POST, PUBLIC_MATCHERS).permitAll()
		.antMatchers(HttpMethod.GET, PUBLIC_MATCHERS).permitAll()
		.antMatchers(HttpMethod.PUT, PUBLIC_MATCHERS).permitAll()
		.antMatchers(PUBLIC_MATCHERS).permitAll() 
		.anyRequest().authenticated();
    	
    }
    
//    @Override
//	protected void configure(HttpSecurity http) throws Exception {
//		
//		if (Arrays.asList(env.getActiveProfiles()).contains("test")) {
//            http.headers().frameOptions().disable();
//        }
//		
//		http.cors().and().csrf().disable();
//		http.authorizeRequests()
//			.antMatchers(HttpMethod.POST, PUBLIC_MATCHERS_POST).permitAll()
//			.antMatchers(HttpMethod.GET, PUBLIC_MATCHERS_GET).permitAll()
//			.antMatchers(HttpMethod.PUT, PUBLIC_MATCHERS_PUT).permitAll()
//			.antMatchers(PUBLIC_MATCHERS).permitAll() 
//			.anyRequest().authenticated();
//		http.addFilter(new JWTAuthenticationFilter(authenticationManager(), jwtUtil));
//		http.addFilter(new JWTAuthorizationFilter(authenticationManager(), jwtUtil, userDetailsService));
//		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
//	}

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        UrlBasedCorsConfigurationSource source = new
                UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
        return source;
    }

}
