package planilha.colegiados;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class PontoFocal  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private String codigoAcesso;
	
	private String orgao;
	
	private String email;
	
	public PontoFocal() {
	}

	public PontoFocal(String codigoAcesso, String orgao, String email) {
		super();
		this.codigoAcesso = codigoAcesso;
		this.orgao = orgao;
		this.email = email;
	}

	public String getCodigoAcesso() {
		return codigoAcesso;
	}

	public void setCodigoAcesso(String codigoAcesso) {
		this.codigoAcesso = codigoAcesso;
	}

	public String getOrgao() {
		return orgao;
	}

	public void setOrgao(String orgao) {
		this.orgao = orgao;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoAcesso == null) ? 0 : codigoAcesso.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PontoFocal other = (PontoFocal) obj;
		if (codigoAcesso == null) {
			if (other.codigoAcesso != null)
				return false;
		} else if (!codigoAcesso.equals(other.codigoAcesso))
			return false;
		return true;
	}

}
