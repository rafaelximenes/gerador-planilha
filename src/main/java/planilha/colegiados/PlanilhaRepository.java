package planilha.colegiados;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface PlanilhaRepository extends JpaRepository<Planilha, Integer> {
	
//	@Query("select a from AcaoUsuario a where  a.data = ?1 and a.cliente.id=?2")
//	public Planilha consultaAcaoUsuario(String data, Integer id);
	
	public List<Planilha> findByCodigoAcesso(String codigoAcesso);
	
	public List<Planilha> findDistinctDataInsercaoByCodigoAcessoNot(String codigoAcesso);
	
	@Query("select distinct p.dataEnvio, p.codigoAcesso from Planilha p")
	public List<Planilha> findAllWithDistinct();
	

}

