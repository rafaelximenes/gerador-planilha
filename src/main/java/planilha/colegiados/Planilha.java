package planilha.colegiados;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Planilha implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	private Integer id; //A
	
	private Date dataEnvio; //B
	
	private Integer ultimaPagina; //C
	
	private String codigoAcesso; //E
	
	private String nomeColegiado; //F
	
	private String siglaColegiado; //G
	
	private String codSiorg; //H
	
	private String orgaoCoordenador; //I
	
	private String nomeOrgaoCoordenador; //J
	
	private String  tipoAtoCriacao; //K
	
	private String tipoAtoCriacaoOutros; //L
	
	private String numeroAtoCriacao; //M
	
	private Date dataCriacao; //N
	
	private String areaAtuacao; //O
	
	private String areaAtuacaoOutros; //P
	
	private String tipoColegiado; //Q
	
	private String naturezaColegiado; //R
	
	private String periodicidadeReuniao; //S
	
	
	private String temporario; //V
	
	private Date dataFimVigencia; // W
	
	private Boolean participacaoSociedadeCivil;  //X
	
	private Boolean membrosNatosPreDefinidos; //AA
	
	private String formaDesignacaoRepresentante; //AB
	
	private String dataInsercao;
	
	private String tipoColegiadoOutros;
	
	
	public Planilha() {
	}

	public Planilha(Integer id, Date dataEnvio, Integer ultimaPagina, String codigoAcesso, String nomeColegiado,
			String siglaColegiado, String codSiorg, String orgaoCoordenador, String nomeOrgaoCoordenador,
			String tipoAtoCriacao, String numeroAtoCriacao, Date dataCriacao,
			String areaAtuacao, String areaAtuacaoOutros, String tipoColegiado,String tipoColegiadoOutros ,String naturezaColegiado,
			String temporario, Date dataFimVigencia, Boolean participacaoSociedadeCivil,
			Boolean membrosNatosPreDefinidos, String formaDesignacaoRepresentante) {
		super();
		this.id = id;
		this.dataEnvio = dataEnvio;
		this.ultimaPagina = ultimaPagina;
		this.codigoAcesso = codigoAcesso;
		this.nomeColegiado = nomeColegiado;
		this.siglaColegiado = siglaColegiado;
		this.codSiorg = codSiorg;
		this.orgaoCoordenador = deParaOrgaoCoordenador(orgaoCoordenador);
		this.nomeOrgaoCoordenador = nomeOrgaoCoordenador;
		this.tipoAtoCriacao = deParaTipoAtoCriacao(tipoAtoCriacao);
		this.numeroAtoCriacao = numeroAtoCriacao;
		this.dataCriacao = dataCriacao;
		this.areaAtuacao = deParaAreaAtuacao(areaAtuacao);
		this.areaAtuacaoOutros = areaAtuacaoOutros;
		this.tipoColegiado = deParaTipoColegiado(tipoColegiado);
		this.tipoColegiadoOutros = tipoColegiadoOutros;
		this.temporario = deParaTemporario(temporario);
		this.dataFimVigencia = dataFimVigencia;
		this.participacaoSociedadeCivil = participacaoSociedadeCivil;
		this.membrosNatosPreDefinidos = membrosNatosPreDefinidos;
		this.formaDesignacaoRepresentante = formaDesignacaoRepresentante;
		this.naturezaColegiado = naturezaColegiado;
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		this.dataInsercao = sdf.format(new Date());
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDataEnvio() {
		return dataEnvio;
	}

	public void setDataEnvio(Date dataEnvio) {
		this.dataEnvio = dataEnvio;
	}

	public Integer getUltimaPagina() {
		return ultimaPagina;
	}

	public void setUltimaPagina(Integer ultimaPagina) {
		this.ultimaPagina = ultimaPagina;
	}

	public String getCodigoAcesso() {
		return codigoAcesso;
	}

	public void setCodigoAcesso(String codigoAcesso) {
		this.codigoAcesso = codigoAcesso;
	}

	public String getNomeColegiado() {
		return nomeColegiado;
	}

	public void setNomeColegiado(String nomeColegiado) {
		this.nomeColegiado = nomeColegiado;
	}

	public String getSiglaColegiado() {
		return siglaColegiado;
	}

	public void setSiglaColegiado(String siglaColegiado) {
		this.siglaColegiado = siglaColegiado;
	}

	public String getCodSiorg() {
		return codSiorg;
	}

	public void setCodSiorg(String codSiorg) {
		this.codSiorg = codSiorg;
	}

	public String getOrgaoCoordenador() {
		return orgaoCoordenador;
	}

	public void setOrgaoCoordenador(String orgaoCoordenador) {
		this.orgaoCoordenador = orgaoCoordenador;
	}

	public String getNomeOrgaoCoordenador() {
		return nomeOrgaoCoordenador;
	}

	public void setNomeOrgaoCoordenador(String nomeOrgaoCoordenador) {
		this.nomeOrgaoCoordenador = nomeOrgaoCoordenador;
	}

	public String getTipoAtoCriacao() {
		return tipoAtoCriacao;
	}

	public void setTipoAtoCriacao(String tipoAtoCriacao) {
		this.tipoAtoCriacao = tipoAtoCriacao;
	}

	public String getTipoAtoCriacaoOutros() {
		return tipoAtoCriacaoOutros;
	}

	public void setTipoAtoCriacaoOutros(String tipoAtoCriacaoOutros) {
		this.tipoAtoCriacaoOutros = tipoAtoCriacaoOutros;
	}

	public String getNumeroAtoCriacao() {
		return numeroAtoCriacao;
	}

	public void setNumeroAtoCriacao(String numeroAtoCriacao) {
		this.numeroAtoCriacao = numeroAtoCriacao;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public String getAreaAtuacao() {
		return areaAtuacao;
	}

	public void setAreaAtuacao(String areaAtuacao) {
		this.areaAtuacao = areaAtuacao;
	}

	public String getAreaAtuacaoOutros() {
		return areaAtuacaoOutros;
	}

	public void setAreaAtuacaoOutros(String areaAtuacaoOutros) {
		this.areaAtuacaoOutros = areaAtuacaoOutros;
	}

	public String getTipoColegiado() {
		return tipoColegiado;
	}

	public void setTipoColegiado(String tipoColegiado) {
		this.tipoColegiado = tipoColegiado;
	}

	public String getNaturezaColegiado() {
		return naturezaColegiado;
	}

	public void setNaturezaColegiado(String naturezaColegiado) {
		this.naturezaColegiado = naturezaColegiado;
	}

	public String getPeriodicidadeReuniao() {
		return periodicidadeReuniao;
	}

	public void setPeriodicidadeReuniao(String periodicidadeReuniao) {
		this.periodicidadeReuniao = periodicidadeReuniao;
	}


	public String getTemporario() {
		return temporario;
	}

	public void setTemporario(String temporario) {
		this.temporario = temporario;
	}

	public Date getDataFimVigencia() {
		return dataFimVigencia;
	}

	public void setDataFimVigencia(Date dataFimVigencia) {
		this.dataFimVigencia = dataFimVigencia;
	}

	public Boolean getParticipacaoSociedadeCivil() {
		return participacaoSociedadeCivil;
	}

	public void setParticipacaoSociedadeCivil(Boolean participacaoSociedadeCivil) {
		this.participacaoSociedadeCivil = participacaoSociedadeCivil;
	}

	public Boolean getMembrosNatosPreDefinidos() {
		return membrosNatosPreDefinidos;
	}

	public void setMembrosNatosPreDefinidos(Boolean membrosNatosPreDefinidos) {
		this.membrosNatosPreDefinidos = membrosNatosPreDefinidos;
	}

	public String getFormaDesignacaoRepresentante() {
		return formaDesignacaoRepresentante;
	}

	public void setFormaDesignacaoRepresentante(String formaDesignacaoRepresentante) {
		this.formaDesignacaoRepresentante = formaDesignacaoRepresentante;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Planilha other = (Planilha) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public String getDataInsercao() {
		return dataInsercao;
	}

	public void setDataInsercao(String dataInsercao) {
		this.dataInsercao = dataInsercao;
	}
	
	public String deParaOrgaoCoordenador(String aux) {
		switch(aux) {
		case "A1":
			return "Adminstração Direta";
		case "A2":
			return "Autarquia";
		case "A3":
			return "Fundação";
		default:
			return "";
		}
		
	}
	
	public String deParaTipoAtoCriacao(String aux) {
		switch(aux) {
		case "A1":
			return "Lei";
		case "A2":
			return "Decreto";
		case "A3":
			return "Regulamento";
		case "A4":
			return "Resolução";
		case "A5":
			return "Deliberação";
		case "A6":
			return "Portaria";
		case "A7":
			return "Outros";
		default:
			return "";
		}
		
	}
	
	public String deParaAreaAtuacao(String aux) {
		switch(aux) {
		case "1":
			return "Transparência, Integridade e combate à corrupção";
		case "2":
			return "Nova previdência e política fiscal solvente";
		case "3":
			return "Educação";
		case "4":
			return "Saúde";
		case "5":
			return "Assistência social, cultura e esporte";
		case "6":
			return "Vínculos familiares intergeracionais";
		case "7":
			return "Infraestrutura, transporte, energia e recursos naturais";
		case "8":
			return "Ciência, tecnologia e comunicações";
		case "9":
			return "Defesa";
		case "10":
			return "Fortalecimento das instituições republicanas";
		case "11":
			return "Produtividade e competitividade com geração de oportunidades e empregos";
		case "12":
			return "Desenvolvimento territorial";
		case "13":
			return "Justiça e segurança pública";
		case "14":
			return "Agropecuária e meio ambiente";
		case "15":
			return "Política externa e comércio exterior";
		case "-oth-":
			return "Outros";
		default:
			return "";
		}
	}
	
	public String deParaTipoColegiado(String aux) {
		switch(aux) {
		case "1":
			return "Conselho";
		case "2":
			return "Comitê";
		case "3":
			return "Comissão";
		case "4":
			return "Grupo";
		case "5":
			return "Junta";
		case "6":
			return "Equipes";
		case "7":
			return "Mesa";
		case "8":
			return "Fórum";
		case "9":
			return "Sala";
		case "-oth-":
			return "Outros";
		default:
			return "";
		}
	}
	
	public String deParaNaturezaColegiado(String aux) {
		switch(aux) {
		case "A1":
			return "Deliberativo";
		case "A2":
			return "Consultivo";
		case "A3":
			return "Judicante";
		case "A4":
			return "Outra";
		default:
			return "";
		}
		
	}
	
	public String deParaPeriodicidadeReuniao(String aux) {
		switch(aux) {
		case "A1":
			return "Mensal";
		case "A2":
			return "Bimestral";
		case "A3":
			return "Trimestral";
		case "A4":
			return "Semestral";
		case "A5":
			return "Anual";
		case "A6":
			return "Outra periodicidade";
		case "A7":
			return "Não há reuniões periódicas";
		default:
			return "";
		}
		
	}
	
	public String deParaTemporario(String aux) {
		switch(aux) {
		case "A1":
			return "Temporário";
		case "A2":
			return "Permanente";
		default:
			return "";
		}
		
	}

	public String getTipoColegiadoOutros() {
		return tipoColegiadoOutros;
	}

	public void setTipoColegiadoOutros(String tipoColegiadoOutros) {
		this.tipoColegiadoOutros = tipoColegiadoOutros;
	}
	

}
