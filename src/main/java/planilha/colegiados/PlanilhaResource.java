package planilha.colegiados;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(value = "/planilha")
public class PlanilhaResource {

	public static String nomeArquivoSaida;

	@Value("${diretorio_arquivo}")
	private String pathFileName;

	@Value("${diretorio_arquivo_gerado}")
	private String pathOutputFile;

	@Autowired
	private PlanilhaService planilhaService;

	private BufferedReader br;

	private static File file;

	private static  byte[] fileContent;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> listar(@PathVariable Integer id) {
		Planilha planilha = planilhaService.findById(id);
		if (planilha == null) {
			return ResponseEntity.badRequest().body("Planilha inexistente");
		} else {
			return ResponseEntity.ok().body(planilha);
		}
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Planilha>> findAll() {
		List<Planilha> list = planilhaService.findAll();
		List<Planilha> auxList = new ArrayList<>();
		for (Planilha p : list) {
			Planilha aux = new Planilha();
			aux.setDataEnvio(p.getDataEnvio());
			aux.setDataInsercao(p.getDataInsercao());
			aux.setCodigoAcesso(p.getCodigoAcesso());
			auxList.add(aux);
		}
		return ResponseEntity.ok(auxList);
	}

	@PostMapping({ "/api/upload" })
	public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile uploadfile) {
		if (uploadfile.isEmpty()) {
			return ResponseEntity.ok().body("please select a file!");
		}

		try {
			saveUploadedFiles(Arrays.asList(new MultipartFile[] { uploadfile }));

			converteXslsEmXML();
		} catch (IOException e) {
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}

		return ResponseEntity.ok().body("Successfully uploaded - " + uploadfile.getOriginalFilename());
	}

	private static String formatElement(String prefix, String tag, String value) {
		StringBuilder sb = new StringBuilder(prefix);
		sb.append("<");
		sb.append(tag);
		if (value != null && value.length() > 0) {
			sb.append(">");
			sb.append(value);
			sb.append("</");
			sb.append(tag);
			sb.append(">");
		} else {
			sb.append("/>");
		}
		return sb.toString();
	}

	public void converteXslsEmXML() {
		PrintWriter out = null;

		try {
			InputStream inputStream = new FileInputStream(new File(this.pathFileName));
			Workbook wb = WorkbookFactory.create(inputStream);
			Sheet sheet = wb.getSheet("Plan1");
			
			FileWriter fostream = new FileWriter(this.pathOutputFile + nomeArquivoSaida + ".xml");
			out = new PrintWriter(new BufferedWriter(fostream));

			out.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			out.println("<DECISOES_CMRI>");

			boolean firstRow = true;
			for (Row row : sheet) {
				if (firstRow == true) {
					firstRow = false;
					continue;
				}
				out.println("\t<DECISAO_CMRI>");
				out.println(formatElement("\t\t", "NUM_REUNIAO", formatCell(row.getCell(0))));
				out.println(formatElement("\t\t", "NUM_DECISAO", formatCell(row.getCell(1))));
				out.println(formatElement("\t\t", "DATA_DECISAO", formatCell(row.getCell(2))));
				out.println(formatElement("\t\t", "RECURSO_NUP", formatCell(row.getCell(3))));
				out.println(formatElement("\t\t", "RECORRENTE", formatCell(row.getCell(4))));
				out.println(formatElement("\t\t", "REQUERIDA", formatCell(row.getCell(5))));

				out.println("\t\t<Relatorio>");
				out.println(formatElement("\t\t\t", "RESUMO_DO_PEDIDO_ORIGINAL", formatCell(row.getCell(6))));
				out.println(formatElement("\t\t\t", "RAZOES_DO_ORGAO_ENTIDADE_REQUERIDA", formatCell(row.getCell(7))));
				out.println(formatElement("\t\t\t", "DECISAO_DA_CGU", formatCell(row.getCell(8))));
				out.println(formatElement("\t\t\t", "RAZOES_DO_RECORRENTE", formatCell(row.getCell(9))));
				out.println("\t\t</Relatorio>");

				out.println(
						formatElement("\t\t", "ANALISE_DE_ADMISSIBILIDADE_DO_RECURSO", formatCell(row.getCell(10))));
				out.println(formatElement("\t\t", "ANALISE_DO_MERITO", formatCell(row.getCell(11))));
				out.println(formatElement("\t\t", "DECISAO", formatCell(row.getCell(12))));
				out.println(formatElement("\t\t", "PROVIDENCIAS", formatCell(row.getCell(13))));

				out.println("\t</DECISAO_CMRI>");
			}
			out.write("</DECISOES_CMRI>");
			out.flush();
			out.close();
			file = new File(this.pathOutputFile + nomeArquivoSaida + ".xml");
			fileContent = Files.readAllBytes(file.toPath());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@GetMapping("/api/file")
	public ResponseEntity<byte[]> getFile()
			throws Exception {
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getName() + "\"")
				.body(fileContent);

	}

	@PostMapping("/api/create_files")
	public ResponseEntity<?> createFiles() throws Exception {
		List<PontoFocal> listPontoFocal = this.planilhaService.findAllPontoFocal();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		String dataAtual = sdf.format(new Date());

		new File(pathOutputFile + dataAtual).mkdirs();
		for (PontoFocal pf : listPontoFocal) {
			List<Planilha> listColegiadosPorOrgao = this.planilhaService.findByCodigoAcesso(pf.getCodigoAcesso());
			new File(pathOutputFile + dataAtual + File.separator + pf.getCodigoAcesso() + ".csv").createNewFile();
			FileWriter csvWriter = new FileWriter(
					pathOutputFile + dataAtual + File.separator + pf.getCodigoAcesso() + ".csv");
			csvWriter.append("Data do envio");
			csvWriter.append(",");
			csvWriter.append("nomeColegiado");
			csvWriter.append(",");
			csvWriter.append("siglaColegiado");
			csvWriter.append(",");
			csvWriter.append("codSiorg");
			csvWriter.append(",");
			csvWriter.append("orgaoCoordenador");
			csvWriter.append(",");
			csvWriter.append("nomeOrgaoCoordenador");
			csvWriter.append(",");
			csvWriter.append("tipoAtoCriacao");
			csvWriter.append(",");
			csvWriter.append("numeroAtoCriacao");
			csvWriter.append(",");
			csvWriter.append("dataCriacao");
			csvWriter.append(",");
			csvWriter.append("areaAtuacao");
			csvWriter.append(",");
			csvWriter.append("areaAtuacaoOutros");
			csvWriter.append(",");
			csvWriter.append("tipoColegiado");
			csvWriter.append(",");
			csvWriter.append("naturezaColegiado");
			csvWriter.append(",");
			csvWriter.append("periodicidadeReuniao");
			csvWriter.append(",");
			csvWriter.append("secretariaExecutiva");
			csvWriter.append(",");
			csvWriter.append("permiteSubcolegiados");
			csvWriter.append(",");
			csvWriter.append("temporario");
			csvWriter.append(",");
			csvWriter.append("dataFimVigencia");
			csvWriter.append(",");
			csvWriter.append("participacaoSociedadeCivil");
			csvWriter.append(",");
			csvWriter.append("exigeQuorumReuniao");
			csvWriter.append(",");
			csvWriter.append("exigeQuorumDeliberacao");
			csvWriter.append(",");
			csvWriter.append("membrosNatosPreDefinidos");
			csvWriter.append(",");
			csvWriter.append("formaDesignacaoRepresentante");
			csvWriter.append("\n");

			for (Planilha p : listColegiadosPorOrgao) {
				csvWriter.append(p.getDataEnvio() + "");
				csvWriter.append(",");
				csvWriter.append(p.getNomeColegiado());
				csvWriter.append(",");
				csvWriter.append(p.getSiglaColegiado());
				csvWriter.append(",");
				csvWriter.append(p.getCodSiorg());
				csvWriter.append(",");
				csvWriter.append(p.getOrgaoCoordenador()); // Administração Direta, Autarquia, Fundação
				csvWriter.append(",");
				csvWriter.append(p.getNomeOrgaoCoordenador());
				csvWriter.append(",");
				csvWriter.append(p.getTipoAtoCriacao()); // Lei, Decreto, Regulamento, Resolução, Deliberação, Portaria,
															// Outros
				csvWriter.append(",");
				csvWriter.append(p.getTipoAtoCriacaoOutros());
				csvWriter.append(",");
				csvWriter.append(p.getNumeroAtoCriacao());
				csvWriter.append(",");
				csvWriter.append(p.getDataCriacao() + "");
				csvWriter.append(",");
				csvWriter.append(p.getAreaAtuacao());
				csvWriter.append(",");
				csvWriter.append(p.getAreaAtuacaoOutros());
				csvWriter.append(",");
				csvWriter.append(p.getTipoColegiado());
				csvWriter.append(",");
				csvWriter.append(p.getNaturezaColegiado()); // Deliberativo, Consultivo, Judicante, Outra
				csvWriter.append(",");
				csvWriter.append(p.getPeriodicidadeReuniao()); // Mensal, Bimestral, Trimestral, Semestral, Anual, Outra
																// Periodicidade, Não há reuniões periódicas
				csvWriter.append(",");
//				csvWriter.append(p.getSecretariaExecutiva());
//				csvWriter.append(",");  
//				csvWriter.append(p.getPermiteSubcolegiados()+"");  
//				csvWriter.append(",");  
				csvWriter.append(p.getTemporario() + ""); // Temporário, Permanente
				csvWriter.append(",");
				csvWriter.append(p.getDataFimVigencia() + "");
				csvWriter.append(",");
				csvWriter.append(p.getParticipacaoSociedadeCivil() + "");
				csvWriter.append(",");
//				csvWriter.append(p.getExigeQuorumReuniao()+"");
//				csvWriter.append(",");  
//				csvWriter.append(p.getExigeQuorumDeliberacao()+"");  
//				csvWriter.append(",");  
				csvWriter.append(p.getMembrosNatosPreDefinidos() + "");
				csvWriter.append(",");
				csvWriter.append(p.getFormaDesignacaoRepresentante());
				csvWriter.append("\n");
			}

			csvWriter.flush();
			csvWriter.close();

		}

		return ResponseEntity.ok().body("Successfully uploaded - ");

	}

	@PostMapping("/api/process_file")
	public ResponseEntity<?> processFile() {
		this.planilhaService.deleteAll();
		try {
			br = new BufferedReader(new FileReader(pathFileName));
			String line;
			int i = 0;
			while ((line = br.readLine()) != null) {
				if (i != 0) {
					System.out.println(line);
					String[] values = line.split(",");
					String aux[] = new String[values.length];
					int j = 0;
					for (String s : values) {
						s = s.replaceAll("\"", "");
						aux[j] = s;
						j++;
					}
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date d1, d2, d3 = null;
					try {
						SimpleDateFormat s = new SimpleDateFormat("dd/MM/yyyy");
						d1 = sdf.parse(aux[1]);
						String aux1 = s.format(d1);
						d1 = s.parse(aux1);
					} catch (Exception ee) {
						d1 = null;
					}
					try {
						d2 = sdf.parse(aux[17]);
					} catch (Exception ee) {
						d2 = null;
					}
					try {
						d3 = sdf.parse(aux[27]);
					} catch (Exception ee) {
						d3 = null;
					}
					Boolean b1, b2;
					try {
						b1 = convertStringToBoolean(aux[28]);
						b2 = convertStringToBoolean(aux[29]);
					} catch (Exception ee) {
						break;
					}
					Integer i1, i2 = null;
					try {
						i1 = Integer.parseInt(aux[0]);
					} catch (Exception ee) {
						i1 = null;
						break;
					}
					try {
						i2 = Integer.parseInt(aux[2]);
					} catch (Exception ee) {
						i2 = null;
					}
					String nomeOrgao = retornaNomeOrgao(aux[12], aux[13], aux[14]);
					String naturezaColegiado = retornaNaturezaColegiado(aux[22], aux[23], aux[24], aux[25]);
					Planilha planilha = new Planilha(i1, d1, i2, aux[4], aux[9], aux[10], aux[11], aux[12], nomeOrgao,
							aux[15], aux[16], d2, aux[18], aux[19], aux[20], aux[21], naturezaColegiado, aux[26], d3,
							b1, b2, aux[30]);
					this.planilhaService.inserirPlanilha(planilha);

				}
				i++;
			}
		} catch (Exception e) {
			System.out.println("Erro ao ler arquivo");
		}

		return ResponseEntity.ok().body("Successfully uploaded - ");

	}

	// save file
	private void saveUploadedFiles(List<MultipartFile> files) throws IOException {
		File f = new File(this.pathFileName);
		if (f.exists()) {
			f.delete();
		}

		for (MultipartFile file : files) {
			nomeArquivoSaida = file.getOriginalFilename();
			if (file.isEmpty()) {
				continue;
			}
			byte[] bytes = file.getBytes();
			Path path = Paths.get(this.pathFileName, new String[0]);
			Files.write(path, bytes, new java.nio.file.OpenOption[0]);
		}
	}

	private Boolean convertStringToBoolean(String aux) {
		if (aux == null || aux.isEmpty()) {
			return null;
		}
		if (aux.equalsIgnoreCase("Y")) {
			return true;
		} else {
			return false;
		}
	}

	// Ordem de chamada dos servicos
//  1) Upload arquivo
//	2) processar arquivo
//  3) monta arquivo de resposta por orgao  
//	4) enviar email

	public void processarArquivo() throws IOException {
		this.planilhaService.deleteAll();
		try {
			br = new BufferedReader(new FileReader(pathFileName));
			String line;
			int i = 0;
			while ((line = br.readLine()) != null) {
				if (i != 0) {
					System.out.println(line);
					String[] values = line.split(",");
					if (!line.contains(",")) {
						break;
					}
					String aux[] = new String[values.length];
					int j = 0;
					for (String s : values) {
						s = s.replaceAll("\"", "");
						aux[j] = s;
						j++;
					}
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date d1, d2, d3 = null;
					try {
						SimpleDateFormat s = new SimpleDateFormat("dd/MM/yyyy");
						d1 = sdf.parse(aux[1]);
						String aux1 = s.format(d1);
						d1 = s.parse(aux1);
					} catch (Exception ee) {
						d1 = null;
					}
					try {
						d2 = sdf.parse(aux[17]);
					} catch (Exception ee) {
						d2 = null;
					}
					try {
						d3 = sdf.parse(aux[27]);
					} catch (Exception ee) {
						d3 = null;
					}
					Boolean b1, b2;
					try {
						b1 = convertStringToBoolean(aux[28]);
						b2 = convertStringToBoolean(aux[29]);
					} catch (Exception ee) {
						break;
					}
					Integer i1, i2 = null;
					try {
						i1 = Integer.parseInt(aux[0]);
					} catch (Exception ee) {
						i1 = null;
						break;
					}
					try {
						i2 = Integer.parseInt(aux[2]);
					} catch (Exception ee) {
						i2 = null;
					}
					String nomeOrgao = retornaNomeOrgao(aux[12], aux[13], aux[14]);
					String naturezaColegiado = retornaNaturezaColegiado(aux[22], aux[23], aux[24], aux[25]);
					Planilha planilha = new Planilha(i1, d1, i2, aux[4], aux[9], aux[10], aux[11], aux[12], nomeOrgao,
							aux[15], aux[16], d2, aux[18], aux[19], aux[20], aux[21], naturezaColegiado, aux[26], d3,
							b1, b2, aux[30]);
					this.planilhaService.inserirPlanilha(planilha);

				}
				i++;
			}
		} catch (Exception e) {
			System.out.println("Erro ao ler arquivo");
		}
	}

	private String retornaNaturezaColegiado(String string, String string2, String string3, String string4) {
		String retorno = "";
		if (string != null && !string.isEmpty() && string.equalsIgnoreCase("Y")) {
			retorno += "Consultivo;";
		}
		if (string2 != null && !string2.isEmpty() && string2.equalsIgnoreCase("Y")) {
			retorno += "Deliberativo;";
		}
		if (string3 != null && !string3.isEmpty() && string3.equalsIgnoreCase("Y")) {
			retorno += "Judicante;";
		}
		if (string3 != null && !string3.isEmpty()) {
			retorno += string4;
		}
		if (retorno.length() > 0) {
			retorno = retorno.substring(0, retorno.length() - 1);
		}
		return retorno;
	}

	private String retornaNomeOrgao(String aux1, String aux2, String aux3) {
		if (aux1 != null && !aux1.isEmpty()) {
			if (aux1.equalsIgnoreCase("A1")) {
				return "Nome do Orgão";
			}
			if (aux1.equalsIgnoreCase("A2")) {
				return aux2;
			}
			if (aux1.equalsIgnoreCase("A3")) {
				return aux3;
			}
		}
		return "";
	}

	public void criarArquivosExcel() {
		try {
			List<PontoFocal> listPontoFocal = this.planilhaService.findAllPontoFocal();
			SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
			String dataAtual = sdf.format(new Date());
			File index = new File(pathOutputFile + dataAtual);
			if (index.exists()) {
				String[] entries = index.list();
				for (String s : entries) {
					File currentFile = new File(index.getPath(), s);
					currentFile.delete();
				}
			} else {
				new File(pathOutputFile + dataAtual).mkdirs();
			}

			for (PontoFocal pf : listPontoFocal) {
				List<Planilha> listColegiadosPorOrgao = this.planilhaService.findByCodigoAcesso(pf.getCodigoAcesso());
				int rowCount = 0;
				if (!listColegiadosPorOrgao.isEmpty()) {
					XSSFWorkbook workbook = new XSSFWorkbook();
					XSSFSheet sheet = workbook.createSheet("Colegiados");
					int columnCount = 0;
					Row row = sheet.createRow(rowCount);

					CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
					Font font = sheet.getWorkbook().createFont();
					font.setBoldweight(Font.BOLDWEIGHT_BOLD);
					cellStyle.setFont(font);

					Cell cell = row.createCell(columnCount);
					cell.setCellValue("Data de envio");
					cell.setCellStyle(cellStyle);
					columnCount++;
					cell = row.createCell(columnCount);
					cell.setCellValue("Nome do Colegiado");
					cell.setCellStyle(cellStyle);
					columnCount++;
					cell = row.createCell(columnCount);
					cell.setCellValue("Sigla do Colegiado");
					cell.setCellStyle(cellStyle);
					columnCount++;
					cell = row.createCell(columnCount);
					cell.setCellValue("Código Siorg");
					cell.setCellStyle(cellStyle);
					columnCount++;
					cell = row.createCell(columnCount);
					cell.setCellValue("Responsável pela administração do colegiado");
					cell.setCellStyle(cellStyle);
					columnCount++;
					cell = row.createCell(columnCount);
					cell.setCellValue("Órgão/Área/Unidade responsável pela coordenação");
					cell.setCellStyle(cellStyle);
					columnCount++;
					cell = row.createCell(columnCount);
					cell.setCellValue("Tipo de Ato de criação");
					cell.setCellStyle(cellStyle);
					columnCount++;
					cell = row.createCell(columnCount);
					cell.setCellValue("Número do ato de criação");
					cell.setCellStyle(cellStyle);
					columnCount++;
					cell = row.createCell(columnCount);
					cell.setCellValue("Data do ato de criação");
					cell.setCellStyle(cellStyle);
					columnCount++;
					cell = row.createCell(columnCount);
					cell.setCellValue("Área de atuação");
					cell.setCellStyle(cellStyle);
					columnCount++;
					cell = row.createCell(columnCount);
					cell.setCellValue("Área de Atuação[Outros]");
					cell.setCellStyle(cellStyle);
					columnCount++;
					cell = row.createCell(columnCount);
					cell.setCellValue("Tipo de colegiado");
					cell.setCellStyle(cellStyle);
					columnCount++;
					cell = row.createCell(columnCount);
					cell.setCellValue("Tipo de colegiado[Outros]");
					cell.setCellStyle(cellStyle);
					columnCount++;
					cell = row.createCell(columnCount);
					cell.setCellValue("Natureza do colegiado");
					cell.setCellStyle(cellStyle);
					columnCount++;
					cell = row.createCell(columnCount);
					cell.setCellValue("Temporario ou permanete?");
					cell.setCellStyle(cellStyle);
					columnCount++;
					cell = row.createCell(columnCount);
					cell.setCellValue("Data fim da vigência");
					cell.setCellStyle(cellStyle);
					columnCount++;
					cell = row.createCell(columnCount);
					cell.setCellValue("Possui participação da sociedade civil?");
					cell.setCellStyle(cellStyle);
					columnCount++;
					cell = row.createCell(columnCount);
					cell.setCellValue("Membros natos pré-definidos?");
					cell.setCellStyle(cellStyle);
					columnCount++;
					cell = row.createCell(columnCount);
					cell.setCellValue("Forma de designação de membro representante");
					cell.setCellStyle(cellStyle);
					rowCount++;
					for (Planilha p : listColegiadosPorOrgao) {
						columnCount = 0;
						row = sheet.createRow(rowCount);
						cell = row.createCell(columnCount);
						cell.setCellValue(dataToStringFormatada(p.getDataEnvio()));
						columnCount++;
						cell = row.createCell(columnCount);
						cell.setCellValue(p.getNomeColegiado() == null ? "" : p.getNomeColegiado());
						columnCount++;
						cell = row.createCell(columnCount);

						cell.setCellValue(p.getSiglaColegiado() == null ? "" : p.getSiglaColegiado());
						columnCount++;
						cell = row.createCell(columnCount);
						cell.setCellValue(p.getCodSiorg() == null ? "" : p.getCodSiorg());
						columnCount++;
						cell = row.createCell(columnCount);
						cell.setCellValue(p.getOrgaoCoordenador() == null ? "" : p.getOrgaoCoordenador());
						columnCount++;
						cell = row.createCell(columnCount);
						cell.setCellValue(p.getNomeOrgaoCoordenador() == null ? "" : p.getNomeOrgaoCoordenador());
						columnCount++;
						cell = row.createCell(columnCount);
						cell.setCellValue(p.getTipoAtoCriacao() == null ? "" : p.getTipoAtoCriacao());
						columnCount++;
						cell = row.createCell(columnCount);
						cell.setCellValue(p.getNumeroAtoCriacao() == null ? "" : p.getNumeroAtoCriacao());
						columnCount++;
						cell = row.createCell(columnCount);
						cell.setCellValue(dataToStringFormatada(p.getDataCriacao()));
						columnCount++;
						cell = row.createCell(columnCount);
						cell.setCellValue(p.getAreaAtuacao() == null ? "" : p.getAreaAtuacao());
						columnCount++;
						cell = row.createCell(columnCount);
						cell.setCellValue(p.getAreaAtuacaoOutros() == null ? "" : p.getAreaAtuacaoOutros());
						columnCount++;
						cell = row.createCell(columnCount);
						cell.setCellValue(p.getTipoColegiado() == null ? "" : p.getTipoColegiado());
						columnCount++;
						cell = row.createCell(columnCount);
						cell.setCellValue(p.getTipoColegiadoOutros() == null ? "" : p.getTipoColegiadoOutros());
						columnCount++;
						cell = row.createCell(columnCount);
						cell.setCellValue(p.getNaturezaColegiado() == null ? "" : p.getNaturezaColegiado());
						columnCount++;
						cell = row.createCell(columnCount);
						cell.setCellValue(p.getTemporario() == null ? "" : p.getTemporario() + "");
						columnCount++;
						cell = row.createCell(columnCount);
						cell.setCellValue(dataToStringFormatada(p.getDataFimVigencia()));
						columnCount++;
						cell = row.createCell(columnCount);
						cell.setCellValue(booleanToStringFormatada(p.getParticipacaoSociedadeCivil()));
						columnCount++;
						cell = row.createCell(columnCount);
						cell.setCellValue(booleanToStringFormatada(p.getMembrosNatosPreDefinidos()));
						columnCount++;
						cell = row.createCell(columnCount);
						cell.setCellValue(
								p.getFormaDesignacaoRepresentante() == null ? "" : p.getFormaDesignacaoRepresentante());
						columnCount++;
						rowCount++;
					}
					try (FileOutputStream outputStream = new FileOutputStream(
							pathOutputFile + dataAtual + File.separator + pf.getCodigoAcesso() + ".xlsx")) {
						workbook.write(outputStream);
					}

				}
			}
		} catch (Exception e) {

		}

	}

	public void criaArquivos() throws IOException {
		List<PontoFocal> listPontoFocal = this.planilhaService.findAllPontoFocal();
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
		String dataAtual = sdf.format(new Date());
		File index = new File(pathOutputFile + dataAtual);
		if (index.exists()) {
			String[] entries = index.list();
			for (String s : entries) {
				File currentFile = new File(index.getPath(), s);
				currentFile.delete();
			}
		} else {
			new File(pathOutputFile + dataAtual).mkdirs();
		}

		for (PontoFocal pf : listPontoFocal) {
			List<Planilha> listColegiadosPorOrgao = this.planilhaService.findByCodigoAcesso(pf.getCodigoAcesso());
			if (!listColegiadosPorOrgao.isEmpty()) {
				new File(pathOutputFile + dataAtual + File.separator + pf.getCodigoAcesso() + ".csv").createNewFile();
				FileWriter csvWriter = new FileWriter(
						pathOutputFile + dataAtual + File.separator + pf.getCodigoAcesso() + ".csv");
				csvWriter.append("Data de envio");
				csvWriter.append(",");
				csvWriter.append("Nome do Colegiado");
				csvWriter.append(",");
				csvWriter.append("Sigla do Colegiado");
				csvWriter.append(",");
				csvWriter.append("Código Siorg");
				csvWriter.append(",");
				csvWriter.append("Responsável pela administração do colegiado");
				csvWriter.append(",");
				csvWriter.append("Órgão/Área/Unidade responsável pela coordenação");
				csvWriter.append(",");
				csvWriter.append("Tipo de Ato de criação");
				csvWriter.append(",");
				csvWriter.append("Número do ato de criação");
				csvWriter.append(",");
				csvWriter.append("Data do ato de criação");
				csvWriter.append(",");
				csvWriter.append("Área de atuação");
				csvWriter.append(",");
				csvWriter.append("Área de Atuação[Outros]");
				csvWriter.append(",");
				csvWriter.append("Tipo de colegiado");
				csvWriter.append(",");
				csvWriter.append("Tipo de colegiado[Outros]"); // adicionado
				csvWriter.append(",");
				csvWriter.append("Natureza do colegiado");
				csvWriter.append(",");
//				csvWriter.append("Periodicidade de reunião");  
//				csvWriter.append(",");  
//				csvWriter.append("Secretaria-executiva do colegiado");
//				csvWriter.append(",");  
//				csvWriter.append("Permite constituir subcolegiados");  
//				csvWriter.append(",");  
				csvWriter.append("Temporario ou permanete?");
				csvWriter.append(",");
				csvWriter.append("Data fim da vigência");
				csvWriter.append(",");
				csvWriter.append("Possui participação da sociedade civil?");
				csvWriter.append(",");
//				csvWriter.append("Exige quórum para reunião?");
//				csvWriter.append(",");  
//				csvWriter.append("Exige quórum para Deliberacao");  
//				csvWriter.append(",");  
				csvWriter.append("Membros natos pré-definidos");
				csvWriter.append(",");
				csvWriter.append("Forma de designação de membro representante");
				csvWriter.append("\n");

				for (Planilha p : listColegiadosPorOrgao) {
					csvWriter.append(dataToStringFormatada(p.getDataEnvio()));
					csvWriter.append(",");
					csvWriter.append(p.getNomeColegiado() == null ? "" : p.getNomeColegiado());
					csvWriter.append(",");
					csvWriter.append(p.getSiglaColegiado() == null ? "" : p.getSiglaColegiado());
					csvWriter.append(",");
					csvWriter.append(p.getCodSiorg() == null ? "" : p.getCodSiorg());
					csvWriter.append(",");
					csvWriter.append(p.getOrgaoCoordenador() == null ? "" : p.getOrgaoCoordenador()); // Administração
																										// Direta,
																										// Autarquia,
																										// Fundação
					csvWriter.append(",");
					csvWriter.append(p.getNomeOrgaoCoordenador() == null ? "" : p.getNomeOrgaoCoordenador());
					csvWriter.append(",");
					csvWriter.append(p.getTipoAtoCriacao() == null ? "" : p.getTipoAtoCriacao()); // Lei, Decreto,
																									// Regulamento,
																									// Resolução,
																									// Deliberação,
																									// Portaria, Outros
					csvWriter.append(",");
					csvWriter.append(p.getNumeroAtoCriacao() == null ? "" : p.getNumeroAtoCriacao());
					csvWriter.append(",");
					csvWriter.append(dataToStringFormatada(p.getDataCriacao()));
					csvWriter.append(",");
					csvWriter.append(p.getAreaAtuacao() == null ? "" : p.getAreaAtuacao());
					csvWriter.append(",");
					csvWriter.append(p.getAreaAtuacaoOutros() == null ? "" : p.getAreaAtuacaoOutros());
					csvWriter.append(",");
					csvWriter.append(p.getTipoColegiado() == null ? "" : p.getTipoColegiado());
					csvWriter.append(",");
					csvWriter.append(p.getTipoColegiadoOutros() == null ? "" : p.getTipoColegiadoOutros());
					csvWriter.append(",");
					csvWriter.append(p.getNaturezaColegiado() == null ? "" : p.getNaturezaColegiado()); // Deliberativo,
																										// Consultivo,
																										// Judicante,
																										// Outra
					csvWriter.append(",");
//					csvWriter.append(p.getPeriodicidadeReuniao()==null?"":p.getPeriodicidadeReuniao()); //Mensal, Bimestral, Trimestral, Semestral, Anual, Outra Periodicidade, Não há reuniões periódicas  
//					csvWriter.append(",");  
////					csvWriter.append(p.getSecretariaExecutiva()==null?"":p.getSecretariaExecutiva());
//					csvWriter.append(",");  
//					csvWriter.append(booleanToStringFormatada(p.getPermiteSubcolegiados()));  
//					csvWriter.append(",");  
					csvWriter.append(p.getTemporario() == null ? "" : p.getTemporario() + ""); // Temporário, Permanente
					csvWriter.append(",");
					csvWriter.append(dataToStringFormatada(p.getDataFimVigencia()));
					csvWriter.append(",");
					csvWriter.append(booleanToStringFormatada(p.getParticipacaoSociedadeCivil()));
//					csvWriter.append(",");  
//					csvWriter.append(booleanToStringFormatada(p.getExigeQuorumReuniao()));
//					csvWriter.append(",");  
//					csvWriter.append(booleanToStringFormatada(p.getExigeQuorumDeliberacao()));  
					csvWriter.append(",");
					csvWriter.append(booleanToStringFormatada(p.getMembrosNatosPreDefinidos()));
					csvWriter.append(",");
					csvWriter.append(
							p.getFormaDesignacaoRepresentante() == null ? "" : p.getFormaDesignacaoRepresentante());
					csvWriter.append("\n");

				}
				csvWriter.flush();
				csvWriter.close();
			}

		}
	}

	private String booleanToStringFormatada(Boolean aux) {
		if (aux == null) {
			return "Sem resposta";
		}
		if (aux)
			return "SIM";
		else
			return "NÃO";
	}

	private String dataToStringFormatada(Date dataEnvio) {
		if (dataEnvio == null)
			return "";
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return sdf.format(dataEnvio);
	}

	static DecimalFormat df = new DecimalFormat("#####0");

	private static String formatCell(Cell cell) {
		if (cell == null) {
			return "";
		}
		switch (cell.getCellType()) {
		case 3:
			return "";
		case 4:
			return Boolean.toString(cell.getBooleanCellValue());
		case 5:
			return "*error*";
		case 0:
			return df.format(cell.getNumericCellValue());
		case 1:
			return cell.getStringCellValue();
		}
		return "<unknown value>";
	}

}
