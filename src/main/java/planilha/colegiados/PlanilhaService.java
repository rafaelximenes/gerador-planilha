package planilha.colegiados;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlanilhaService {
	
	@Autowired
	private PlanilhaRepository planilhaRepository;
	
	@Autowired
	private PontoFocalRepository pontoFocalRepository;
	
	public void inserirPlanilha(Planilha planilha) {
		planilhaRepository.save(planilha);
	}
	
	public Planilha findById(Integer id) {
		Optional<Planilha> p = this.planilhaRepository.findById(id);
		return p.orElse(null);
	}
	
	public List<Planilha> findAll() {
		List<Planilha> aux= this.planilhaRepository.findAll();
		List<Planilha> listRetorno = new ArrayList<>();
		for(Planilha p: aux) {
			if(listRetorno.isEmpty()) {
				listRetorno.add(p);
			}else {
				boolean adiciona = true;
				for(Planilha pp: listRetorno) {
					if(datasIguais(p.getDataEnvio(), pp.getDataEnvio()) && p.getCodigoAcesso().equalsIgnoreCase(pp.getCodigoAcesso())) {
						adiciona = false;
						break;
					}
				}
				if(adiciona) {
					listRetorno.add(p);
				}
			}
		}
		return listRetorno;
	}
	
	public void deleteAll() {
		this.planilhaRepository.deleteAll();
	}
	
	public List<PontoFocal> findAllPontoFocal() {
		return this.pontoFocalRepository.findAll();
	}
	
	public List<Planilha> findByCodigoAcesso(String codigoAcesso) {
		return this.planilhaRepository.findByCodigoAcesso(codigoAcesso);
	}
	
	public boolean datasIguais(Date d1, Date d2) {
		if(d1==null && d2==null) {
			return true;
		}
		if(d1==null || d2==null) {
			return false;
		}
		if(d1.equals(d2)) {
			return true;
		}else {
			return false;
		}
	}
	

}
